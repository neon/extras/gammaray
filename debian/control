Source: gammaray
Priority: optional
Section: devel
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Jakub Adam <jakub.adam@ktknet.cz>
Build-Depends: cmake,
               debhelper-compat (= 13),
               doxygen,
               gdb,
               graphviz,
               libbfd-dev,
               libdw-dev,
               libdwarf-dev,
               libelf-dev,
               libgraphviz-dev,
               libiberty-dev,
               libproj-dev,
               libwayland-dev,
               kf6-extra-cmake-modules,
               kf6-kcoreaddons-dev,
               kf6-syntax-highlighting-dev,
               mold,
               ninja-build,
               pkg-kde-tools,
               pkg-kde-tools-neon,
               qt6-3d-dev,
               qt6-base-dev,
               qt6-connectivity-dev,
               qt6-declarative-dev,
               qt6-doc-dev,
               qt6-location-dev,
               qt6-positioning-dev,
               qt6-scxml-dev,
               qt6-shadertools-dev,
               qt6-svg-dev,
               qt6-tools-dev,
               qt6-translations-l10n,
               qt6-wayland-dev,
               qt6-webengine-dev [amd64 arm64 armhf i386 mipsel],
               xauth,
               xvfb
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/gammaray.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/gammaray
Homepage: https://www.kdab.com/development-resources/qt-tools/gammaray/

Package: gammaray
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: qt6-3d,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: gammaray-plugin-bluetooth (<= 3.1.0-0neon),
        gammaray-plugin-kjobtracker (<= 3.1.0-0neon),
        gammaray-plugin-positioning (<= 3.1.0-0neon),
        gammaray-plugin-objectvisualizer (<= 3.1.0-0neon),
        gammaray-plugin-quickinspector (<= 3.1.0-0neon),
        gammaray-plugin-waylandinspector (<= 3.1.0-0neon),
Replaces: gammaray-plugin-bluetooth (<= 3.1.0-0neon),
          gammaray-plugin-kjobtracker (<= 3.1.0-0neon),
          gammaray-plugin-positioning (<= 3.1.0-0neon),
          gammaray-plugin-objectvisualizer (<= 3.1.0-0neon),
          gammaray-plugin-quickinspector (<= 3.1.0-0neon),
          gammaray-plugin-waylandinspector (<= 3.1.0-0neon),
Recommends: gdb, lldb
Description: Tool for examining the internals of Qt application
 GammaRay is a tool for examining the internals of a Qt application and
 to some extent also manipulate it. GammaRay uses injection methods to
 hook into an application at runtime and provide access to a wide variety
 of interesting information. It provides easy ways of navigating through
 the complex internal structures you find in some Qt frameworks, such as
 QGraphicsView, model/view, QTextDocument, state machines and more.

Package: gammaray-dev
Architecture: any
Section: libdevel
Depends: gammaray (= ${binary:Version}), 
         qt6-base-dev,
         ${misc:Depends}
Description: GammaRay plugin development files
 GammaRay is a tool for examining the internals of a Qt application and
 to some extent also manipulate it. GammaRay uses injection methods to
 hook into an application at runtime and provide access to a wide variety
 of interesting information. It provides easy ways of navigating through
 the complex internal structures you find in some Qt frameworks, such as
 QGraphicsView, model/view, QTextDocument, state machines and more.
 .
 This package contains header files used for building 3rd party GammaRay
 plugins.
